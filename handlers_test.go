package main

import (
	"net/http"
	"net/http/httptest"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestPingRoute(t *testing.T) {
	// Only pass t into top-level Convey calls
	Convey("Given that you have a webserver", t, func() {
		webServer := webServer()

		Convey("and a HTTP vcr tape", func() {
			resp := httptest.NewRecorder()

			Convey("and that you make a GET request to /ping", func() {
				req, _ := http.NewRequest("GET", "/ping", nil)
				webServer.ServeHTTP(resp, req)

				Convey("You should have recorded a pong!", func() {
					So(resp.Body.String(), ShouldEqual, "pong")

					Convey("And received a status of 200", func() {
						So(resp.Code, ShouldEqual, 200)
					})
				})
			})
		})
	})
}

func BenchmarkPingRoute(b *testing.B) {
	// run the Fib function b.N times
	for n := 0; n < b.N; n++ {
		webServer := webServer()
		resp := httptest.NewRecorder()
		req, _ := http.NewRequest("GET", "/ping", nil)
		webServer.ServeHTTP(resp, req)
	}
}

func TestStatusRoute(t *testing.T) {
	// Only pass t into top-level Convey calls
	Convey("Given that you have a webserver", t, func() {
		webServer := webServer()

		Convey("and a HTTP vcr tape", func() {
			resp := httptest.NewRecorder()

			Convey("and that you make a GET request to /status", func() {
				req, _ := http.NewRequest("GET", "/status", nil)
				webServer.ServeHTTP(resp, req)

				Convey("You should have recorded a success!", func() {
					So(resp.Body.String(), ShouldEqual, "success")

					Convey("And received a status of 200", func() {
						So(resp.Code, ShouldEqual, 200)
					})
				})
			})
		})
	})
}

func BenchmarkStatusRoute(b *testing.B) {
	// run the Fib function b.N times
	for n := 0; n < b.N; n++ {
		webServer := webServer()
		resp := httptest.NewRecorder()
		req, _ := http.NewRequest("GET", "/status", nil)
		webServer.ServeHTTP(resp, req)
	}
}
