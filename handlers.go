package main

import (
	"fmt"
	"net/http"

	"github.com/julienschmidt/httprouter"
)

// Ping handler function for /ping route.
func Ping(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	fmt.Fprint(w, "pong")
}

// Status handler function for /status route.
func Status(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	fmt.Fprint(w, "success")
}
