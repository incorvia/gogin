package main

import (
	"log"
	"net/http"
	"os"

	"github.com/gorilla/handlers"
	"github.com/julienschmidt/httprouter"
)

func main() {
	// listen and server on 0.0.0.0:8080
	log.Fatal(http.ListenAndServe(":8080", webServer()))
}

func webServer() http.Handler {
	router := httprouter.New()

	// Routes
	router.GET("/ping", Ping)
	router.GET("/status", Status)

	// Middleware
	loggedRouter := handlers.LoggingHandler(os.Stdout, router)

	// Return
	return loggedRouter
}
